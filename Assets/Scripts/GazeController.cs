﻿using UnityEngine;
using System.Collections;

public class GazeController : MonoBehaviour {
	public delegate void GazeEvent (RaycastHit hit);
	public static event GazeEvent OnGazeEnter;
	public static event GazeEvent OnGazeLeave;


	private bool enterEventSent = false;
	private bool leaveEventSent = false;

	RaycastHit lastHit;

	private Camera c;

	// Use this for initialization
	void Start () {
		Init ();
	}

	void Init() {
		c = gameObject.GetComponent<Camera> ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		RaycastHit hit = new RaycastHit ();

		Transform TransformCamera = c.gameObject.transform;

		Ray MyRay = new Ray (TransformCamera.position, TransformCamera.forward);

		if (Physics.Raycast (MyRay, out hit)) {
			if (hit.collider != null) {
				
				if (hit.collider.tag == "knob") {
					lastHit = hit;
					leaveEventSent = false;
					hit.collider.GetComponent<ButtonController> ().mButtonState = ButtonController.ButtonState.Highlighted;
					if (!enterEventSent) {
						OnGazeEnter (hit);
						enterEventSent = true;
					}
				} 
			} 

		} else {
			enterEventSent = false;
			if(!leaveEventSent) {
				OnGazeLeave (lastHit);
				leaveEventSent = true;
			}
		}
	}
}
