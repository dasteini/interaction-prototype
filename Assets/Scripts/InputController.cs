﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour {
	

	// Use this for initialization
	void Start () {
		Init ();
	}

	void Init() {
		GazeController.OnGazeEnter += OnGazeEnter;
		GazeController.OnGazeLeave += OnGazeLeave;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	
	}

	void OnGazeEnter(RaycastHit hit) {
		Debug.Log ("*InputController* Gazed at " + hit.collider.tag);
		if (Input.GetKeyDown(KeyCode.C)) {
			hit.collider.GetComponent<ButtonController> ().mButtonState = ButtonController.ButtonState.Pressed;
		}
	}

	void OnGazeLeave(RaycastHit hit) {
		Debug.Log ("*InputController* Stopped gazing.");
	}
}
