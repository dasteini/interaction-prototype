﻿using UnityEngine;
using System.Collections;

public class ButtonController : MonoBehaviour {
	public bool isHighlighted = false;
	public ButtonState mButtonState = ButtonState.Normal;
	private Renderer mRender;

	public  enum ButtonState{Normal, Highlighted, Pressed};

	// Use this for initialization
	void Start () {
		mRender = gameObject.GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		/*
		if (isHighlighted) {
			mRender.material.color = Color.green;
		} else {
			mRender.material.color = Color.black;
		}
		*/

		if (mButtonState == ButtonState.Highlighted) {
			mRender.material.color = Color.green;
		} else if (mButtonState == ButtonState.Pressed) {
			mRender.material.color = Color.cyan;
		} else if (mButtonState == ButtonState.Normal) {
			mRender.material.color = Color.black;
		}
		mButtonState = ButtonState.Normal;
		//isHighlighted = false;
	}

}
