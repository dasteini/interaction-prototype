﻿using UnityEngine;
using System.Collections;

public class GazeScript : MonoBehaviour {
	public bool isNearToConsole = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		RaycastHit hit;
		Ray clickRay = new Ray (transform.position, Vector3.forward);
		print (transform.position);

		if(isNearToConsole) {
			if(Physics.Raycast(clickRay, out hit, 10.0f)) {
				if (hit.collider.tag == "knob") {
					print ("Button!!");
				}
			}
		}
	}
}
